import typing

from starlette_oauth2 import AuthenticateMiddleware
from starlette.config import Config
from starlette.applications import Starlette
from starlette.middleware.sessions import SessionMiddleware
from starlette.responses import JSONResponse
from starlette.config import Config
from starlette.datastructures import Secret


# declare a database. SQLAlchemy is an option: the DB just needs the interface declared below.
from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy import Column, String, JSON
from sqlalchemy.orm import create_session
from sqlalchemy.ext.automap import automap_base


class DB:
    def __init__(self) -> None:
        self._engine = create_engine('sqlite:///users.db')
        _metatable = Table('users', MetaData(bind=self._engine), *[
            Column('id', String, primary_key=True, nullable=False),
            Column('token', JSON),
        ])
        _metatable.create(checkfirst=True)

        Base = automap_base(metadata=_metatable.metadata)
        Base.prepare()

        self._session = create_session(bind=self._engine)
        self._User = Base.classes.users

    def put(self, user_id: str, token: typing.Dict[str, typing.Any]) -> None:
        self._session.merge(self._User(id=user_id, token=dict(token)))
        self._session.flush()

    def get(self, user_id: str) -> typing.Optional[typing.Dict[str, typing.Any]]:
        result = self._session.query(self._User).filter(self._User.id == user_id).first()
        if result is not None:
            return result.token

    def delete(self, user_id: str) -> None:
        self._session.query(self._User).filter(self._User.id == user_id).delete()
        self._session.flush()


# configuration
config = Config('.env')

config.SECRET_KEY = config('SECRET_KEY', cast=Secret)
config.SERVER_METADATA_URL = config('SERVER_METADATA_URL', cast=str)
config.CLIENT_ID = config('CLIENT_ID', cast=str)
config.CLIENT_SECRET = config('CLIENT_SECRET', cast=Secret)

# declaration of the app and middlewares
app = Starlette()

# if db is None, the token is stored in the session instead.
# if db is None, the `SECRET_KEY` MUST BE very secret or someone can impersonate the user.
db = DB()


class AuthenticateMiddleware(AuthenticateMiddleware):
    # make the path `/public` public
    PUBLIC_PATHS = {'/public'}

## order is important here because session must be available to the AuthenticateMiddleware
app.add_middleware(AuthenticateMiddleware,
    db=db,
    server_metadata_url=config.SERVER_METADATA_URL,
    client_id=config.CLIENT_ID,
    client_secret=config.CLIENT_SECRET,
    force_https_redirect=False,  # make it True if your load balancer strips the "s" when forwarding requests to your app.
)
app.add_middleware(SessionMiddleware, secret_key=config.SECRET_KEY)


@app.route('/other')
async def homepage(request):
    user = request.session.get('user')
    return JSONResponse(user)


@app.route('/public')
async def homepage(request):
    user = request.session.get('user')
    if user is None:
        payload = {"message": "User not authenticated"}
    else:
        payload = user
    return JSONResponse(payload)


if __name__ == '__main__':
    import uvicorn
    import logging
    import sys
    # log requests to the identity provider
    logger = logging.getLogger('httpx')
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler(sys.stdout))
    uvicorn.run(app, host='localhost', port=5001)
